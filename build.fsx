#load "tools/includes.fsx"
open IntelliFactory.Build

let bt =
    BuildTool().PackageId("PROJECTNAME")
        .VersionFrom("WebSharper")
        .WithFSharpVersion(FSharpVersion.FSharp30)
        .WithFramework(fun f -> f.Net40)

let main =
    bt.WebSharper.PROJECTTYPE("PROJECTNAME")
        .SourcesFromProject()
        .Embed([])
        .References(fun r -> [])

let tests =
    bt.WebSharper.SiteletWebsite("PROJECTNAME.Tests")
        .SourcesFromProject()
        .Embed([])
        .References(fun r ->
            [
                r.Project(main)
                r.NuGet("WebSharper.Testing").Reference()
                r.NuGet("WebSharper.UI.Next").Reference()
            ])

bt.Solution [
    main
    tests

    bt.NuGet.CreatePackage()
        .Configure(fun c ->
            { c with
                Title = Some "PROJECTTITLE"
                LicenseUrl = Some "http://websharper.com/licensing"
                ProjectUrl = Some "https://github.com/intellifactory/PROJECTREPO"
                Description = "PROJECTDESCR"
                RequiresLicenseAcceptance = true })
        .Add(main)
]
|> bt.Dispatch
