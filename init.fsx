open System
open System.IO

type Names =
    {
        ProjectName : string
        ProjectTitle : string
        ProjectRepo : string
        ProjectDescr : string
        ProjectType : string
    }

let ifEmpty def s =
    if String.IsNullOrWhiteSpace s then def else s

let readNames() =
    printfn "Project name? (as a valid identifier)"
    printfn "  example: WebSharper.GlMatrix"
    let projectName =
        Console.ReadLine().Trim()
    if String.IsNullOrWhiteSpace projectName then
        printfn "Aborting."
        exit 1
    printfn "Project title?"
    printfn "  example: WebSharper.GlMatrix-2.2.0"
    printfn "  default: %s" projectName
    let projectTitle =
        Console.ReadLine().Trim()
        |> ifEmpty projectName
    printfn "Project description?"
    printfn "  example: WebSharper Extensions for glMatrix 2.2.0"
    printfn "  default: %s" projectTitle
    let projectDescr =
        Console.ReadLine().Trim()
        |> ifEmpty projectTitle
    let defaultRepo =
        "https://github.com/intellifactory/"
        + projectName.ToLowerInvariant()
    printfn "Project repository URL?"
    printfn "  default: %s" defaultRepo
    let projectRepo =
        Console.ReadLine().Trim()
        |> ifEmpty defaultRepo
    let rec getType() =
        printfn "Project type? 1: Library, 2: WIG Extension"
        printfn "  default: 1"
        match Console.ReadLine().Trim() with
        | "" | "1" -> "Library"
        | "2" -> "Extension"
        | _ -> printfn "Invalid project type."; getType()
    let projectType = getType()
    {
        ProjectName = projectName
        ProjectTitle = projectTitle
        ProjectDescr = projectDescr
        ProjectRepo = projectRepo
        ProjectType = projectType
    }

let replaceStr (name: string) (value: string) (s: string) =
    s
        .Replace("Z" + name, value.Replace("WebSharper", "Zafir"))
        .Replace(name, value)

let subst names (s: string) =
    s
    |> replaceStr "PROJECTNAME" names.ProjectName
    |> replaceStr "PROJECTLOWNAME" (names.ProjectName.ToLowerInvariant())
    |> replaceStr "PROJECTTITLE" names.ProjectTitle
    |> replaceStr "PROJECTREPO" names.ProjectRepo
    |> replaceStr "PROJECTDESCR" names.ProjectDescr
    |> replaceStr "PROJECTTYPE" names.ProjectType

let replace names =
    printfn "  Renaming PROJECTNAME/ to %s/" names.ProjectName
    Directory.Move("PROJECTNAME", names.ProjectName)
    printfn "  Renaming PROJECTNAME.Tests/ to %s.Tests/" names.ProjectName
    Directory.Move("PROJECTNAME.Tests", names.ProjectName + ".Tests")
    File.Delete("README.md")
    let allFiles =
        Directory.GetFiles(".", "*", SearchOption.AllDirectories)
        |> Array.filter (fun f ->
            not (
                f.Contains "tools" // avoid NuGet.exe binary
             || f.Contains ".git"  // avoid .git directory
            )
        )
    for f in allFiles do
        let dir = Path.GetDirectoryName(f)
        let filename = Path.GetFileName(f)
        let doReplace newFilename =
            let newF = Path.Combine(dir, newFilename)
            printfn "  Updating %s as %s" f newF
            let content = File.ReadAllText f
            File.WriteAllText(newF, subst names content)
            if filename <> newFilename then File.Delete(f)
        match Path.GetExtension(filename).ToLowerInvariant(), names.ProjectType with
        | ".lib", "Library"
        | ".wig", "Extension"
        | ".any", _ ->
            doReplace <| subst names (Path.GetFileNameWithoutExtension filename)
        | ".lib", _
        | ".wig", _ ->
            File.Delete(f)
        | _ -> doReplace <| subst names filename

printfn "Substituting variables..."
readNames() |> replace
printfn "Done."
printfn "Running initial build..."
System.Diagnostics.Process.Start(
    System.Diagnostics.ProcessStartInfo("build.cmd", CreateNoWindow = true)
).WaitForExit()
printfn "Done."
File.Delete("init.fsx")
File.Delete("init.cmd")
